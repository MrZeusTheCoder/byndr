import optparse
from io import BytesIO
from os import listdir
from tokenize import Triple
from types import *
from typing import List

from pdfrw import PageMerge, PdfReader, PdfWriter
from reportlab.lib.pagesizes import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.graphics.shapes import *

pdfmetrics.registerFont(TTFont("Outfit", "Outfit-SemiBold.ttf"))
LEFT_CORDS = (.15 * inch, 5.30 * inch)
RIGHT_CORDS = (4.1 * inch, 5.30 * inch)

loaded_pages = []

loaded_pdfs: List[PdfReader] = []

#Merges on page1. I.e. use page1's pdf
def merge_page(page1, page2):
    merger = PageMerge(page1)
    merger.add(page2).render()

def get_page_num_pdf(page_num):
    pdf_bytes = BytesIO()
    c = canvas.Canvas(pdf_bytes, pagesize=(4.25 * inch, 5.5 * inch))
    c.setFont("Outfit", 8)

    pos = ()
    if(page_num % 2 == 0):
        pos = LEFT_CORDS
    else:
        pos = RIGHT_CORDS

    if(page_num % 4 == 2):
        c.setFillColor("Black")
        c.rect(0, 5.2 * inch, 0.3 * inch, 0.3 * inch, fill=1)
        c.setFillColor("White")
        
    c.drawCentredString(pos[0], pos[1], str(page_num))
    c.showPage()
    c.save()
    pdf_bytes.seek(0)
    return PdfReader(pdf_bytes)

def pack_four(pages):
    da_page = PageMerge() + pages
    x_increment, y_increment = (i for i in da_page.xobj_box[2:])
    for i, page in enumerate(da_page):
        page.x = x_increment if i & 1 else 0
        page.y = 0 if i & 2 else y_increment
    return da_page.render()


def fourUp(all_pages):
    return [pack_four(all_pages[i:i+4]) for i in range(0, len(all_pages), 4)]


def bynd_fourUp():
    if total_pages % 2 > 0:
        print("WARNING: MUST BE MULTIPLE OF FOUR FOR FOUR_UP TO WORK PROPERLY!")
    if total_pages % 4 > 0:
        print("WARNING: THERE WILL BE MISSING MISSING PAGES. MUST BE MULTIPLE OF FOUR FOR FOUR_UP TO WORK PROPERLY!")

    front_pages = []
    back_pages = []
    for i in range(int(len(loaded_pages)/4)):
        front_pages.append(loaded_pages.pop())
        front_pages.append(loaded_pages.pop(0))
        back_pages.append(loaded_pages.pop(0))
        back_pages.append(loaded_pages.pop())

    front_pages = fourUp(front_pages)
    back_pages = fourUp(back_pages)

    for i, x in enumerate(front_pages):
        writer = PdfWriter(fname=f"{i}.pdf")
        writer.addpage(x)
        writer.write()

    for i, x in enumerate(back_pages):
        writer = PdfWriter(fname=f"{i}b.pdf")
        writer.addpage(x)
        writer.write()
    print("Finished bynding with fourUp.")


def bynd_serial():
    writer = PdfWriter()
    writer.addpages(loaded_pages)
    buf = BytesIO()
    writer.write(buf)
    buf.seek(0)
    pdf = PdfReader(buf)
    for i, x in enumerate(pdf.pages[2:-2]):
        merge_page(x, get_page_num_pdf(i+1).pages[0])
    PdfWriter().write("all.pdf", pdf)
    print("Finished serial bynding.")


if __name__ == "__main__":
    parser = optparse.OptionParser(description="A PDF booklet binding script.")
    parser.add_option("-i", "--input-dir", help="Path to the directory of the input files.",
                      action="store_const", dest="input_dir", default="./indexed")
    parser.add_option("-4", "--4up", help="Make a four-panel booklet PDF from the input files.",
                      action="store_true", dest="fourUp")
    parser.add_option("-s", "--serial", help="Make a PDF from each indexed page in order.",
                      action="store_true", dest="serial")
    (options, args) = parser.parse_args()

    input_dir = options.input_dir

    dir = [x for x in listdir(input_dir) if x.isnumeric()]
    dir.sort(key=int)

    for file in dir:
        print(f"-> Loading file {file}.")
        loaded_pdfs.append(PdfReader(f"{input_dir}/{file}"))
    print()

    total_pages = 0
    for x in loaded_pdfs:
        loaded_pages.extend(x.pages)
        total_pages += len(x.pages)

    if options.fourUp:
        bynd_fourUp()
    elif options.serial:
        bynd_serial()
    else:
        print("Please select a bynding option. (\"--4up\", \"--serial\", etc.)")
    bynd_serial()